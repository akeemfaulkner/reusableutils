/**
 * Identity function
 *
 * @param value
 *
 * @return {*}
 */
const id = (value) => value;

/**
 * Accepts an invariable number of comma separated arguments
 * returns true if all arguments are equal
 *
 * @param {*} args
 *
 * @return {Boolean}
 *
 */
const eq = (...args) => args.reduce((a, b) => a === b) !== false;

/**
 * Accepts an invariable number of comma separated functions.
 * Returns a new function that will apply all the functions from left to right
 *
 * @param {Function} funcs
 *
 */
const pipe = (...funcs) => {
    const _pipe = (a, b) => (...args) => b(a(...args));
    return funcs.reduce(_pipe);
};

/**
 * Accepts an invariable number of comma separated functions.
 * Returns a new function that will apply all the functions from right to left
 *
 * @param {Function} funcs
 *
 */
export const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

const negate = (value) => !value;

const typeOf = (type, value) => typeof value === type;

const isUndefined = (value) => typeOf(value, 'undefined');

const isDefined = (value) => negate(isUndefined(value));

/**
 * Returns a list of unique values
 * @param {[*]} list
 * @returns {[*]}
 */
const unique = (list) => list.reduce((p, c) => p.includes(c) ? [].concat(p) : [].concat(p, [c]), []);

const isNull = (value) => value === null;

const notNull = (value) => negate(isNull(value));

const getTruthy = (...args) => args.reduce((a, b) => a || b, false);

export function arrayToKeyValue(array = [], key) {
    let result = {};
    array.forEach(function (value) {
        result[value[key]] = value;
    });

    return result;
}

export function forEachValue(object, callback) {
    for (let key in object) {
        if (object.hasOwnProperty(key)) {
            callback(object[key], key, object);
        }
    }
}

const MATCH_ARRAY_INDEX = /\[(\d)\]/; // regex to match on array index accessor e.g 'a[2]'

/**
 * takes a string accessor key and finds its value on an object
 * returns undefined if the key does not exist
 * @param {String} key
 * @param {Object} object
 */
function getProp(key, object) {
    const accessors = key ? key.split('.') : [];
    return accessors.reduce((value, accessor) => {


        const accessorKeys = accessor.split(MATCH_ARRAY_INDEX).filter(isNonEmptyString); // filter out empty keys;

        return accessorKeys.reduce((accessorValue, currentKey) => { // reduce up the accessor accessorKeys to get the value

            // if there is a accessorValue get the value at key
            return accessorValue ? accessorValue[currentKey] : /** @type {undefined} */ accessorValue;
        }, value);
    }, object);
}


export function setValue(object, path, value) {
    const accessors = path.split(/\./).filter(a => a !== '');
    const isObjectAccessor = accessor => isNaN(parseInt(accessor));
    const getKeys = accessor => accessor.split(MATCH_ARRAY_INDEX).filter(isNonEmptyString); // filter out empty keys;;

    for (let i = 0; i < accessors.length - 1; i++) {
        let accessor = accessors[i];
        let keys = getKeys(accessor);
        keys.forEach(key=> {
            if (key in object) {
                object = object[key];
            } else {
                object[key] = isObjectAccessor(accessor) ? {} : [];
                object = object[key];
            }
        })
    }
    object[accessors[accessors.length - 1]] = value;
}

export function mapData(mappingConfig, state, updates, compareFn) {
    let hasUpdates = false;
    for (let key in mappingConfig) {
        if (mappingConfig.hasOwnProperty(key)) {
            const updateValueKey = mappingConfig[key];
            const oldValue = getProp(key, state);
            const newValue = getProp(updateValueKey, updates);
            const compareFnHasChanged = compareFn && !compareFn(oldValue, newValue);
            const simpleComparisonHasChanges = !compareFn && oldValue !== newValue;
            const hasUpdate = compareFnHasChanged || simpleComparisonHasChanges;

            if (updateValueKey && hasUpdate) {
                setValue(state, key, newValue);
                hasUpdates = true;
            }
        }
    }
    return hasUpdates;
}

const getPropOr = (object, key, or) => getProp(key, object) || or;

const or = (a, b) => a || b;

const pascalToWords = words => words && words.match(/[A-Z0-9][a-z]*/g).join(' ');

const capitaliseString = word => word[0].toUpperCase() + word.substr(1);

const decapitaliseString = word => word[0].toLowerCase() + word.substr(1);

const snakeToCamelCase = (str) => {
    let result = str.toLowerCase();
    result = result.split('_');
    result = result.map(capitaliseString).join('');
    return decapitaliseString(result);
};

const noop = (...args) => void 0;

const copy = (value) => {
    try {
        return JSON.parse(JSON.stringify(value));
    } catch (error) {
        return value;
    }
};

const getFromListExcluding = (condition, list) => list.filter(item => !condition(item));

const difference = (excludingValues, array) => array.filter(el => !excludingValues.includes(el));

const forEach = (array, onArrayValue, breakOn = noop) => {
    for (let i = 0; i < array.length; i++) {
        onArrayValue(array[i], i, array);
        if (breakOn(array[i], i, array)) {
            return array[i];
        }
    }
};

/**
 * Gets the query string of the window location object and returns a key value map
 * @return {{}}
 */
const parseQuery = () => {

    const query = window.location.search.substr(1);

    return query.split('&').map(q => {

        const keyPair = q.split('=');

        return [keyPair[0], keyPair[1]];

    }).reduce((map, keyPairTuple) => {

        const key = keyPairTuple[0];
        const value = keyPairTuple[1];

        const mapValue = map[key];

        if (isDefined(mapValue)) {

            const isArray = mapValue instanceof Array;

            map[key] = isArray ? mapValue.concat([value]) : [mapValue, value];

        } else {
            map[key] = value;
        }

        return map;

    }, {});
};


function createAction(type) {
    return (payload) => ({payload, type});
}

function createArray(initialValues, length) {
    return new Array(length).fill(initialValues);
}

function duplicateValues(list) {
    const duplicates = [];
    const values = [];
    list.forEach(item => {
        if (values.includes(item)) {
            duplicates.push(item);
        }
        values.push(item);
    });
    return duplicates;
}

export function objectWalker(object, find) {
    let result;
    for (let key in object) {
        if (object.hasOwnProperty(key)) {
            const value = object[key];
            if (find(value, key, object)) {
                result = value;
                break;
            }
            if (typeof value === "object" && value.toString() === "[object Object]") {
                result = objectWalker(value, find, [key]);
                if (result) break;
            }
        }
    }
    return result;
}

function clone(object) {
    if (object === null || typeof(object) !== 'object' || '$__isClone__$' in object) {
        return object;
    }

    const isDate = object instanceof Date;
    const temp = isDate ? new object.constructor() : object.constructor();

    for (let key in object) {
        if (Object.prototype.hasOwnProperty.call(object, key)) {
            object['$__isClone__$'] = null;
            temp[key] = clone(object[key]);
            delete object['$__isClone__$'];
        }
    }

    return temp;
}
